//Kausar Mussa
//Student ID: 1738212
package lab02;

public class BikeStore {


	public static void main(String[] args) {
		Bicycle[] stock = new Bicycle [4];
		
		stock[0] = new Bicycle ("Trek", 22, 40);
		stock[1] = new Bicycle ("BMC", 20, 45);
		stock[2] = new Bicycle ("Trek", 24, 35);
		stock[3] = new Bicycle ("BMC", 18, 42);
		
		//loop to show output of the stock array
		for (int i = 0; i<4;i++) {
			System.out.println(stock[i].toString());
	}


}
}