//Kausar Mussa
//Student ID: 1738212
package lab02;

public class Bicycle {
//initializing private fields
	
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	// get methods
public String getManufacturer() {
	return this.manufacturer; 
	}
public int getNumberGears() {
	return this.numberGears;
	}
public double getMaxSpeed() {
	return this.maxSpeed;
	}

//constuctor of the class
public Bicycle (String manufacturer, int numberGears, double maxSpeed){
	this.manufacturer = manufacturer;
	this.numberGears = numberGears;
	this.maxSpeed = maxSpeed; }

//overriding the toString() method
 public String toString() {
	 return "Manufacturer: " + manufacturer +", " + "Number of Gears: "  + numberGears+", " + "MaxSpeed: " + maxSpeed;
	 
 }
}
 


